let visibleSlide = 0;

const prevSlide = () => {
  const slides = document.querySelectorAll(".slide");
  visibleSlide--;

  if (visibleSlide < 0) {
    visibleSlide = slides.length - 1;
  }

  for (let i = 0; i < slides.length; i++) {
    slides[i].style.transform = `translateX(-${visibleSlide * 100}%)`;
  }
};

const nextSlide = () => {
  const slides = document.querySelectorAll(".slide");
  visibleSlide++;

  if (visibleSlide >= slides.length) {
    visibleSlide = 0;
  }

  for (let i = 0; i < slides.length; i++) {
    slides[i].style.transform = `translateX(-${visibleSlide * 100}%)`;
  }
};
const COLORS = ['var(--red)', 'var(--green)', 'yellow']

const changeColor = () => {
  const balls = document.querySelectorAll('.green-glow-ball')
  const currentColor = balls[0].style.backgroundColor || "var(--green)";
  const newColorIdx = Math.floor(Math.random()*(COLORS.length-1))
  const newColor = COLORS.filter(m => m !== currentColor)[newColorIdx]

  balls.forEach(ball => {
    ball.style.backgroundColor = newColor
  });
}


document.addEventListener('mousemove', (e) => {
  const trail = document.createElement('div')
  trail.className = 'trail'

  trail.style.backgroundColor = COLORS[Math.floor(Math.random()*COLORS.length)]
  trail.style.left = `${e.clientX}px`
  trail.style.top = `${e.clientY}px`
  document.querySelector('body').appendChild(trail)

  setTimeout(() => {
    trail.remove()
  }, 2000);
})